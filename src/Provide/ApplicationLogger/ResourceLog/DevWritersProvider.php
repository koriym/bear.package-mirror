<?php
/**
 * This file is part of the BEAR.Package package
 *
 * @license http://opensource.org/licenses/bsd-license.php BSD
 */
namespace BEAR\Package\Provide\ApplicationLogger\ResourceLog;

use Aura\Sql\ExtendedPdo;
use BEAR\Package\Provide\ApplicationLogger\ResourceLog\Writer\Collection;
use BEAR\Package\Provide\ApplicationLogger\ResourceLog\Writer\Db;
use BEAR\Package\Provide\ApplicationLogger\ResourceLog\Writer\Fire;
use BEAR\Package\Provide\ApplicationLogger\ResourceLog\Writer\Zf2Log;
use BEAR\Package\Provide\ApplicationLogger\ResourceLog\Writer\Zf2DbLogProvider;
use BEAR\Sunday\Inject\LogDirInject;
use Ray\Di\ProviderInterface;
use Ray\Di\Di\Inject;
use Ray\Di\Di\Named;

/**
 * Writer provider
 */
class DevWritersProvider implements ProviderInterface
{

    use LogDirInject;

    private $resourceDbFile;

//    /**
//     * @Inject
//     * @Named("resource_db")
//     */
//    public function __construct($resourceDbFile)
//    {
//        $this->resourceDbFile = $resourceDbFile;
//    }

    /**
     * @return Collection
     */
    public function get()
    {
        $writers = new Collection(
            [
                new Fire,
                new Zf2Log(new Zf2DbLogProvider("{$this->logDir}")),
                new Db(new ExtendedPdo("sqlite:{$this->logDir}/resource.db}"))
            ]
        );

        return $writers;
    }
}
