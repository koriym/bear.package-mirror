<?php
/**
 * This file is part of the BEAR.Package package
 *
 * @license http://opensource.org/licenses/bsd-license.php BSD
 */
namespace BEAR\Package\Provide\ApplicationLogger\ResourceLog\Writer;

use Aura\Sql\ExtendedPdo;
use BEAR\Resource\LogWriterInterface;
use BEAR\Resource\RequestInterface;
use BEAR\Resource\ResourceObject;
use Ray\Di\ProviderInterface;

/**
 * Db logger
 */
final class Db implements LogWriterInterface, \Serializable
{
    const SQL_INSERT_LOG = 'INSERT INTO log (id, message, extra_page) VALUES (:id, :message, :extra_page)';
    const SQL_CREATE_LOG = 'CREATE TABLE IF NOT EXISTS log(id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, timestamp, message, priority, priorityName, extra_page)';

    /**
     * @var \Aura\Sql\ExtendedPdo
     */
    private $db;

        /**
     * @var string
     */
    private $pageId;

    /**
     * @param \Ray\Di\ProviderInterface $provider
     *
     * @Inject
     */
    public function __construct(ExtendedPdo $db)
    {
        $this->db = $db;
        // create table
        $this->db->exec(self::SQL_CREATE_LOG);
    }

    /**
     * {@inheritdoc}
     */
    public function write(RequestInterface $request, ResourceObject $result)
    {
        $this->pageId = rtrim(base64_encode(pack('H*', md5($_SERVER['REQUEST_TIME_FLOAT']))), '=');
        $id = "{$this->pageId}";
        /** @var $logger \Zend\Log\LoggerInterface */
        $msg = "id:{$id}\treq:" . $request->toUriWithMethod();
        $msg .= "\tcode:" . $result->code;
        $msg .= "\tbody:" . json_encode($result->body);
        $msg .= "\theader:" . json_encode($result->headers);
        $path = $this->getPath(isset($_SERVER['PATH_INFO']));
        $msg .= "\tpath:$path";
        // log


        $this->db->bindValues([
            'id' => $id,
            'message' => $msg,
            'extra_page' => $this->pageId
        ]);
        $this->db->exec(self::SQL_INSERT_LOG);
    }

    /**
     * @param $hasServerInfo
     *
     * @return string
     */
    private function getPath($hasServerInfo)
    {
        if (!$hasServerInfo) {
            return '/';
        }
        $path = $_SERVER['PATH_INFO'];
        $path .= $_GET ? '?' : '';
        $path .= http_build_query($_GET);

        return $path;
    }

    public function serialize()
    {
        $dsn = $this->db->getDsn();
//        unset($this->db);

        return $dsn;
    }

    public function unserialize($dsn)
    {
        $this->db = new ExtendedPdo($dsn);
    }
}
